<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Социальная сеть </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=197" type="text/css"> 
 </head>
 <body>
<?php
  if(empty($_SESSION['auth'])){
	  echo '<div class = "alert">
	        <h4>Чтобы просматривать эту страницу, нужно зайти на сайт.</h4>
			<br>
            <p><a href = "admin/register.php">Регистрация</a></p>
			<br>
            <p><a href = "admin/login.php">Авторизация</a></p>
            </div>';} else {
				
	if(isset($_GET['id'])){
    $id = $_GET['id'];
    $_SESSION['id'] = $_GET['id'];
    } else {
    $id = $_SESSION['id'];
    }
	
	include('baza.php');
	
	$query = "SELECT login FROM society_users WHERE id='$id'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$his_login = mysqli_fetch_assoc($result)['login']; 
?>
  <div class = "wrapper">
   <div class = "wrapper_two">   
	   <main class = "primary_one">
		<div class = "sidebar">
		 <p><a href = "/">Моя страница</a></p>
		 <p><a href = "#">Новости и уведомления</a></p>
		 <p><a href = "chat.php">Мессенджер</a></p>
		 <p><a href = "#">Друзья</a></p>
		 <p><a href = "#">Сообщества</a></p>
		 <p><a href = "#">Фотографии</a></p>
		</div> 
	   </main>	
	   <main class = "primary_two">
		<div class = "primary_foto">
		 <div class = "in_foto"	style ="background-image: url(images/<? 
		 $his_foto = $id;
		 if(file_exists("images/$his_foto.jpg")){
		 echo $his_foto;} else {echo 'standard';}?>.jpg)">
		 </div>
		</div>
<?php
    if(!empty($_GET['add_friend'])){
	$my_id = $_SESSION['user_id'];
	
    include('baza.php');	
	
	$query = "SELECT * FROM society_friends WHERE user_id = '$id' AND friend_id = 0 AND subscriber = '$my_id' AND request_id = '$my_id' AND request_answer = 0";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$one = mysqli_fetch_assoc($result);
	//var_dump($one);
	
	include('baza.php');
	
	$query = "SELECT * FROM society_friends WHERE user_id = '$my_id' AND friend_id = 0 AND subscriber = 0 AND request_id = '$id' AND request_answer = 0";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$two = mysqli_fetch_assoc($result);
	
	if(empty($one) && empty($two)){
		
	include('baza.php');
	
	$query = "INSERT INTO society_friends SET user_id = '$id', friend_id = 0, subscriber = '$my_id', request_id = '$my_id', request_answer = 0";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	
	include('baza.php');
	
	$query = "INSERT INTO society_friends SET user_id = '$my_id', friend_id = 0, subscriber = 0, request_id = '$id', request_answer = 0";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	}
	}			
?>		
		<div class = "friends">
		  <div class = "in_friend">
		   <p><a href = "?&add_friend=<? echo $id; ?>">Добавить в друзья</a></p>
		  </div>
		  <div class = "his_friends">
<?php
    $category = [1 => 'юзер', 2 => 'модератор', 3 => 'администратор'];
	
	if(!empty($_GET['more_his_friends'])){
	echo '<p><a href ="?&less_his_friends=1">Скрыть друзей пользователя</a></p>';}
	if(!empty($_GET['less_his_friends'])){
	echo '<p><a href ="?&more_his_friends=1">Показать друзей пользователя</a></p>';}	
	if(empty($_GET['less_his_friends']) && empty($_GET['more_his_friends'])){	  
	echo '<p><a href ="?&more_his_friends= 1">Показать друзей пользователя</a></p>';}
	
    include('baza.php');
	
	$query = "SELECT friend_id, society_users.login AS friend, society_users.status AS status FROM society_friends 
    LEFT JOIN society_users ON society_friends.friend_id = society_users.id
    WHERE user_id = '$id' AND friend_id > 0
    ";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($his_friends = []; $row = mysqli_fetch_assoc($result); $his_friends[] = $row);
	//var_dump($his_friends);
	
		echo "<div class =\"";
		
			if(!empty($_GET['more_his_friends'])){echo 'show';} else {echo 'hidden';}

		echo "\">";
		
	if(count($his_friends) == 0){echo 'У Пользователя нет друзей';}	
		
	if(count($his_friends) > 0){	
	foreach($his_friends as $friend){
    echo "<div class = \"foto_users\">
				       <div class = \"oval\" style =\"background-image: url(images/";
					   if(file_exists("images/$friend[friend_id].jpg")){
		 echo $friend['friend_id'];} else {echo 'standard';}
		 echo ".jpg)\"></div>
					   <div class = \"right\">
					    <p><a href =\"different_page.php?&id=$friend[friend_id]\" target=\"_blank\">$friend[friend]</a></p>
					    <p>{$category[$friend['status']]}</p>
					   </div>	
					  </div>";
			}
			}
		echo "</div>";			
?>
          </div>
		  <div class = "subscriptions">
<?php
	if(!empty($_GET['more_his_subscribers'])){
	echo '<p><a href ="?&less_his_subscribers=1">Скрыть подписчиков пользователя</a></p>';}
	if(!empty($_GET['less_his_subscribers'])){
	echo '<p><a href ="?&more_his_subscribers=1">Показать подписчиков пользователя</a></p>';}	
	if(empty($_GET['less_his_subscribers']) && empty($_GET['more_his_subscribers'])){	  
	echo '<p><a href ="?&more_his_subscribers= 1">Показать подписчиков пользователя</a></p>';}
	
    include('baza.php');
	
	$query = "SELECT subscriber, society_users.login AS subscribers, society_users.status AS status FROM society_friends 
    LEFT JOIN society_users ON society_friends.subscriber = society_users.id
    WHERE user_id = '$id' AND subscriber > 0
    ";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($his_subscribers = []; $row = mysqli_fetch_assoc($result); $his_subscribers[] = $row);
	//var_dump($his_subscribers);	
	
		echo "<div class =\"";
		
			if(!empty($_GET['more_his_subscribers'])){echo 'show';} else {echo 'hidden';}

		echo "\">";
		
	if(count($his_subscribers) == 0){echo 'У Пользователя нет подписчиков';}	
		
	if(count($his_subscribers) > 0){	
	foreach($his_subscribers as $subscriber){
    echo "<div class = \"foto_users\">
				       <div class = \"oval\" style =\"background-image: url(images/";
					   if(file_exists("images/$subscriber[subscriber].jpg")){
		 echo $subscriber['subscriber'];} else {echo 'standard';}
		 echo ".jpg)\"></div>
					   <div class = \"right\">
					    <p><a href =\"different_page.php?&id=$subscriber[subscriber]\" target=\"_blank\">$subscriber[subscribers]</a></p>
					    <p>{$category[$subscriber['status']]}</p>
					   </div>	
					  </div>";
			}
			}
		echo "</div>";		
?>		  
		  </div>
		</div>
	   </main>	
	   <main class = "primary_three">
		<div class = "info">
		 <p><?php echo $his_login; ?></p>
		 <p class = "language">
		 Язык: 
	<?php
		include('baza.php');
		
		$query = "SELECT main_quote, society_language.language AS my_language, society_political.politics, selfview, society_worldview.view, society_main_thing.things, society_main_in_people.quality, society_smoking.smok, society_alcohol.alco, inspire, quote FROM `society_info` 
		LEFT JOIN society_language ON society_info.language = society_language.id 
		LEFT JOIN society_political ON society_info.political = society_political.id 
		LEFT JOIN society_worldview ON society_info.worldview = society_worldview.id 
		LEFT JOIN society_main_thing ON society_info.main_thing = society_main_thing.id 
		LEFT JOIN society_main_in_people ON society_info.main_in_people = society_main_in_people.id 
		LEFT JOIN society_smoking ON society_info.smoking = society_smoking.id 
		LEFT JOIN society_alcohol ON society_info.alcohol = society_alcohol.id 
		WHERE society_info.user_id = $id";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		$total = mysqli_fetch_assoc($result);
		echo $total['my_language'];
		echo "<p>{$total['main_quote']}</p>";
		//var_dump($total);
	?>	
		</p>
	<?php 
		if(!empty($_GET['more'])){
		echo '<p><a href ="?&less=1">Скрыть</a></p>';}
		if(!empty($_GET['less'])){
		echo '<p><a href ="?&more=1">Показать подробную информацию</a></p>';}
		if(empty($_GET['less']) && empty($_GET['more'])){	  
		echo '<p><a href ="?&more= 1">Показать подробную информацию</a></p>';}	
	?>
		 <div class = "<? if(!empty($_GET['more'])){echo 'show';} else {echo 'hidden';} ?>">
	<?php 
		if((trim($total['politics'])) !== 'Не выбрано'){echo "<p>Политические предпочтения: $total[politics]</p>";}
		if((strlen(trim($total['selfview']))) > 0){echo "<p>Мировоззрение: $total[selfview]</p>";} elseif((trim($total['view'])) != 'Не выбрано' && (strlen(trim($total['selfview']))) < 1) {echo "<p>Мировоззрение: $total[view]</p>";}
		if((trim($total['things'])) != 'Не указано'){echo "<p>Главное в жизни: $total[things]</p>";}
		if((trim($total['quality'])) != 'Не указано'){echo "<p>Главное в людях: $total[quality]</p>";}
		if((trim($total['smok'])) != 'Не указано'){echo "<p>Отношение к курению: $total[smok]</p>";}
		if((trim($total['alco'])) != 'Не указано'){echo "<p>Отношение к алкоголю: $total[alco]</p>";}
		if(strlen(trim($total['inspire'])) > 0){echo "<p>Вдохновляет: $total[inspire]</p>";}
		if(strlen(trim($total['quote'])) > 0){echo "<p>Любимые цитаты: $total[quote]</p>";}
		if(trim($total['politics']) == 'Не выбрано' && trim($total['view']) == 'Не выбрано' && strlen(trim($total['selfview'])) < 1 && trim($total['things'])== 'Не указано' && trim($total['quality']) == 'Не указано' && trim($total['smok']) == 'Не указано' && trim($total['alco']) == 'Не указано' && strlen(trim($total['inspire'])) < 1 && strlen(trim($total['quote'])) < 1){echo '<p>Отсутствует доступная информация</p>';}
	?>
		 </div>
		 <p class = "count_friends"></p>
		 <p class = "count_foto"></p>
		</div> 
		<div class = "my_foto">
		my_foto
		</div>
	<?php 

		if(!empty($_GET['no_what'])){
		echo '<p class ="what_new"><a href ="?&what=1">Что у Вас нового?</a></p>';}
		if(empty($_GET['no_what']) && empty($_GET['what'])){	  
		echo '<p class ="what_new"><a href ="?&what= 1">Напишите что-нибудь...</a></p>';}	
	?>		
		<div class = "news <? if(!empty($_GET['what'])){echo 'show';} else {echo 'hidden';} ?>">
	<?php 
		if(!empty($_GET['what'])){
		echo '<p><a href ="?&no_what=1"><img class = "close" src = "images/krest.png" alt = "close"></a></p>';}
?>		
         <form action="" method="POST">
		  <br>
		  <p><input class = "news_head" type = "text" name = "head" placeholder = "Тема сообщения"></p>
		  <br>
		  <p><textarea class = "news_text" name = "text" placeholder = "Ваше сообщение"></textarea></p>
		  <br>
		  <p><input name="public" type="submit" value="Опубликовать"></p>
		  <br>
		 </form> 
<?php
    if(!empty($_POST['public'])){
	$traveler = $_SESSION['user_id'];
	
    $news_head = $_POST['head'];
	$news_text = $_POST['text'];
	$date = date('Y-m-d H:i:s', time());
	
    include('baza.php');
	
	$query = "INSERT INTO society_bands SET head='$news_head', text='$news_text', date='$date', user_id='$id', author='$traveler'";
    $result = mysqli_query($link, $query) or die(mysqli_error($link)); 
	}	
?>			 
		</div>
	<div class = "band">
<?php
    if(!empty($_POST['answer_band'])){
	$traveler = $_SESSION['user_id'];	
	
	$answer_text = $_POST['answer'];
	$bband = $_POST['bband'];
	$answer_date = date('Y-m-d H:i:s', time());
	
    include('baza.php');

    $query = "INSERT INTO society_comments SET band_id = '$bband', text ='$answer_text', date='$answer_date', user_id='$traveler'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	}
	
    include('baza.php');
	
	$query = "SELECT society_bands.id, head, text, date, society_users.login AS author FROM society_bands  
    LEFT JOIN society_users ON society_bands.author = society_users.id
    WHERE user_id = '$id'
    ORDER BY date DESC";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($bands = []; $rows = mysqli_fetch_assoc($result); $bands[] = $rows);
	//var_dump($bands);
	
	foreach($bands as $band){
		
	$band_id = $band['id'];
	
	include('baza.php');	
	
    $query = "SELECT COUNT(*) AS count FROM society_comments WHERE society_comments.band_id = '$band_id'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$count_comments = mysqli_fetch_assoc($result)['count'];
	//var_dump($count_comments);
	
		echo "<div class = \"ash\">
		       <p>$band[date]</p>
		       <h4>$band[head]</h4>
			   <p>Пользователь: $band[author]</p>
			   <p>$band[text]</p>
			   <p class = \"counter\"><a href = \"?&more_comments=$band[id]\"><img class = \"comments\" src = \"images/comments.png\" alt = \"comments\"></a></p>
			   <p class = \"counter\">"; if($count_comments > 0){echo "$count_comments";} 
			   echo "</p>
			    <div class = "; if(!empty($_GET['more_comments'])){echo 'show';} else {echo 'hidden';}
   
			   echo "\"</div>";
			   
               if(!empty($_GET['more_comments']) && $_GET['more_comments'] == $band['id']){
			   
			   include('baza.php');
			   
			   $query = "SELECT society_comments.text, society_users.login, society_comments.date FROM society_comments 
               LEFT JOIN society_users ON society_comments.user_id = society_users.id
               WHERE society_comments.band_id = '$band_id'
               GROUP BY society_comments.date ASC";
			   $result = mysqli_query($link, $query) or die(mysqli_error($link));
			   for($commentss = []; $rows = mysqli_fetch_assoc($result); $commentss[] = $rows);
			   //var_dump($commentss);
			   
			   if(!empty($commentss)){
				   foreach($commentss as $comment){
					   echo "
					         <div>
					         <p>Дата: $comment[date]</p>
					         <p>Пользователь: $comment[login]</p>
							 <p>Сообщение: $comment[text]</p>
							 </div>";
				   }   echo "<br>
							 <form action=\"\" method=\"POST\">
							  <p><textarea class = \"news_text\" name = \"answer\" placeholder = \"Написать комментарий\"></textarea></p>
							  <br>
							  <p><input name=\"bband\" type=\"hidden\" value=\"$band_id\"></p>
							  <p><input name=\"answer_band\" type=\"submit\" value=\"Ответить\"></p>
							  <br>
							  </form>";
			   } else {echo "Пока данную запись никто не комментировал. Будьте первым!
			                 <br><br>
							 <form action=\"\" method=\"POST\">
							  <p><textarea class = \"news_text\" name = \"answer\" placeholder = \"Ваше сообщение\"></textarea></p>
							  <br>
							  <p><input name=\"bband\" type=\"hidden\" value=\"$band_id\"></p>
							  <p><input name=\"answer_band\" type=\"submit\" value=\"Ответить\"></p>
							  <br>
							  </form>";}
               echo "
			   <p><a href =\"?&less_comments=$band[id]\">Скрыть комментарии</a></p>";}echo "					
			   </div>";
	}	
?>
		</div>	
	   </main>
	   <footer>
		<p><img src = "images/society.png" alt = "society"></p>
		<p>Copyright © 2001 - 2021  Society.yes</p>
	   </footer>
   </div>	   
  </div>
<?php
  }
?>
 </body>   
</html>   