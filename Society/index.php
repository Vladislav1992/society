<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Социальная сеть </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=197" type="text/css"> 
 </head>
 <body>
<?php
  if(empty($_SESSION['auth'])){
	  echo '<div class = "alert">
	        <h4>Чтобы просматривать эту страницу, нужно зайти на сайт.</h4>
			<br>
            <p><a href = "admin/register.php">Регистрация</a></p>
			<br>
            <p><a href = "admin/login.php">Авторизация</a></p>
            </div>';} else {
?>
  <div class = "wrapper">
   <div class = "wrapper_two">   
	   <main class = "primary_one">
		<div class = "sidebar">
		 <p><a href = "/">Моя страница</a></p>
		 <p><a href = "admin/logout.php">Выйти</a></p>
<?php
            $my_foto = $_SESSION['user_id'];
					
			//соглашение с уведомлением о том что меня добавили в друзья
			if(!empty($_GET['ok']) && $_GET['ok'] == 2){
			$yes_friend = $_GET['yes_friend'];
			
			include('baza.php');
			
			$query = "UPDATE society_friends SET request_answer = 0 
			WHERE user_id = '$my_foto' AND friend_id = '$yes_friend' AND request_answer = 1";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//отписка
			if(!empty($_GET['unsubscribe']) && $_GET['unsubscribe'] == 1){
			$unsubscribe_no_friend = $_GET['unsubscribe_no_friend'];
			
			include('baza.php');
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$my_foto' AND request_id = '$unsubscribe_no_friend' AND request_answer = 1";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$unsubscribe_no_friend' AND subscriber = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));			
			}
			
			//соглашение с уведомлением о том что меня оставили в подписчиках
			if(!empty($_GET['ok']) && $_GET['ok'] == 1){
			$no_friend = $_GET['no_friend'];
			
			include('baza.php');
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$my_foto' AND request_id = '$no_friend' AND request_answer = 1";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//отмена заявки в друзья
			if(!empty($_GET['not']) && $_GET['not'] == 1){
			$del_friend = $_GET['del_friend'];
			
			include('baza.php');
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$del_friend' AND subscriber = '$my_foto' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$my_foto' AND request_id = '$del_friend'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//оставить в подписчиках
			if(!empty($_GET['not']) && $_GET['not'] == 2){
			$new_subscriber = $_GET['new_subscriber'];
			
			include('baza.php');
			
			$query = "UPDATE society_friends SET request_id=0 
			WHERE user_id = '$my_foto' AND subscriber = '$new_subscriber' AND request_id = '$new_subscriber'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));

			$query = "UPDATE society_friends SET request_answer=1  
			WHERE user_id = '$new_subscriber' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//добавить в друзья
			if(!empty($_GET['yes']) && $_GET['yes'] == 1){
			$new_friend = $_GET['new_friend'];
			
			include('baza.php');
			
			$query = "UPDATE society_friends SET friend_id = '$new_friend', subscriber=0, request_id=0 
			WHERE user_id = '$my_foto' AND subscriber = '$new_friend' AND request_id = '$new_friend'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			
			$query = "UPDATE society_friends SET friend_id='$my_foto', request_id=0, request_answer = 1 
			WHERE user_id = '$new_friend' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
		if(!empty($_GET['more_news'])){
		echo '<p><a href ="?&less_news=1">Скрыть новости и уведомления></a></p>';}
		if(!empty($_GET['less_news'])){
		echo '<p><a href ="?&more_news=1">Показать новости и уведомления</a></p>';}	
		if(empty($_GET['less_news']) && empty($_GET['more_news'])){	  
		echo '<p><a href ="?&more_news= 1">Показать новости и уведомления</a></p>';}	
		
        include('baza.php');
		
		$query = "SELECT * FROM society_friends WHERE user_id='$my_foto' AND request_id > 0 OR subscriber='$my_foto' AND request_id = '$my_foto' OR user_id='$my_foto' AND request_answer = 1";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
	    for($news = []; $row = mysqli_fetch_assoc($result); $news[] = $row);
		//var_dump($news);

		echo "<div class =\"";
		
			if(!empty($_GET['more_news'])){echo 'show';} else {echo 'hidden';}

		echo "\">";
		
		if(count($news) == 0){
			echo 'Новостей и новых уведомлений нет';
		}
		
		foreach($news as $breeze){
			//меня оставили в подписчиках
			if($breeze['user_id'] == $my_foto && $breeze['request_id'] != $my_foto && $breeze['request_answer'] == 1){
			$other = $breeze['request_id'];
			
			include('baza.php');
			
		    $query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.request_id = society_users.id
			WHERE user_id = '$my_foto' AND request_id = '$other' AND request_answer = 1";
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];	
			
			if(strlen($he) > 0){
			echo "<div> Пользователь <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a> решил оставить Вас в подписчиках
			<p><a href = \"?&ok=1&no_friend=$other\">Ok</a></p>
			<p><a href = \"?&unsubscribe=1&unsubscribe_no_friend=$other\">Отписаться от $he</a></p>
			</div>";
			}			
			}
			
			//меня добавили в друзья
			if($breeze['user_id'] == $my_foto && $breeze['friend_id'] != $my_foto && $breeze['request_answer'] == 1){
			$other = $breeze['friend_id'];
			
			include('baza.php');
			
		    $query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.friend_id = society_users.id
			WHERE user_id = '$my_foto' AND friend_id = '$other' AND request_answer = 1";
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];	
			
			if(strlen($he) > 0){
			echo "<div> Пользователь <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a> добавил Вас в друзья
			<p><a href = \"?&ok=2&yes_friend=$other\">Ok</a></p>
			</div>";	
			}			
			}			
			
			//я подал заявку в друзья
			if($breeze['user_id'] != $my_foto && $breeze['request_id'] == $my_foto && $breeze['request_id'] == $breeze['subscriber']){
			$other = $breeze['user_id'];
			
			include('baza.php');
				
			$query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.user_id = society_users.id
			WHERE user_id = '$other' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];
			
			if(strlen($he) > 0){
			echo "<div> Вы подали заявку в друзья пользователю <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a>
			<p><a href = \"?&not=1&del_friend=$other\">Отменить заявку в друзья</a></p></div>";
			}
			}
			
			//мне подали заявку в друзья
			if($breeze['user_id'] = $my_foto && $breeze['request_id'] != $my_foto && $breeze['request_id'] == $breeze['subscriber']){	
            $other = $breeze['request_id'];		

            include('baza.php');			
			
			$query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.request_id = society_users.id
			WHERE user_id = '$my_foto' AND request_id = '$other'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];
			//var_dump($he);
			
			if(strlen($he) > 0){
			echo "<div> Вам подал заявку в друзья пользователь <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a>
			<p><a href = \"?&not=2&new_subscriber=$other\">Оставить в подписчиках</a></p>
			<p><a href = \"?&yes=1&new_friend=$other\">Добавить в друзья</a></p></div>";
			}
			}			
		}
		
		echo "</div>";	
		
		$category = [1 => 'юзер', 2 => 'модератор', 3 => 'администратор'];
?>
		 <p><a href = "chat.php">Мессенджер</a></p>
		 <p><a href = "#">Друзья</a></p>
		 <p><a href = "#">Сообщества</a></p>
		 <p><a href = "#">Фотографии</a></p>
		 <p><a href = "#">Поиск</a></p>
		 <br>
		 <form action="" method="POST">
		  <p><input name="search_friends" type="text" placeholder="Найти человека"></p>
		  <br>
		  <p><input name="search" type="submit" value="Найти"></p>
		  <br>
		 </form>
<?php
    include('baza.php');
	
    if(!empty($_POST['search_friends'])){
		$name = $_POST['search_friends'];
		
		$query = "SELECT id, login, status FROM society_users WHERE login='$name'";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		for($friends = []; $row = mysqli_fetch_assoc($result); $friends[] = $row);
		
		include('search_friends.php');
		} //else{	
	
    if(isset($_POST['search']) && empty($_POST['search_friends'])){	
		$query = "SELECT id, login, status FROM society_users";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		for($friends = []; $row = mysqli_fetch_assoc($result); $friends[] = $row);
		
		include('search_friends.php');
		}
        //var_dump($friends);	
?>
		</div> 
	   </main>	
	   <main class = "primary_two">
		<div class = "primary_foto">
		 <div class = "in_foto"	style ="background-image: url(images/<? 
		 if(file_exists("images/$my_foto.jpg")){
		 echo $my_foto;} else {echo 'standard';}?>.jpg)">
		 </div>
		</div>		
		<div class = "friends">
		  <div class = "his_friends">
<?php
	if(!empty($_GET['more_his_friends'])){
	echo '<p><a href ="?&less_his_friends=1">Скрыть моих друзей</a></p>';}
	if(!empty($_GET['less_his_friends'])){
	echo '<p><a href ="?&more_his_friends=1">Показать моих друзей</a></p>';}	
	if(empty($_GET['less_his_friends']) && empty($_GET['more_his_friends'])){	  
	echo '<p><a href ="?&more_his_friends= 1">Показать моих друзей</a></p>';}
	
    include('baza.php');
	
	if(!empty($_GET['out_friend'])){
		$out = $_GET['out_friend'];
		
		$query = "DELETE FROM society_friends WHERE user_id='$my_foto' AND friend_id='$out'";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		
		$query = "DELETE FROM society_friends WHERE user_id='$out' AND friend_id='$my_foto'";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
	}
	
	if(!empty($_GET['out_subscriber'])){
		$out = $_GET['out_subscriber'];
		
		$query = "DELETE FROM society_friends WHERE user_id='$my_foto' AND subscriber='$out'";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
	}
	
	$query = "SELECT friend_id, society_users.login AS friend, society_users.status AS status FROM society_friends 
    LEFT JOIN society_users ON society_friends.friend_id = society_users.id
    WHERE user_id = '$my_foto' AND friend_id > 0
    ";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($his_friends = []; $row = mysqli_fetch_assoc($result); $his_friends[] = $row);
	//var_dump($his_friends);
	
		echo "<div class =\"";
		
			if(!empty($_GET['more_his_friends'])){echo 'show';} else {echo 'hidden';}

		echo "\">";
		
	if(count($his_friends) == 0){echo 'У Вас нет друзей';}
	
	if(count($his_friends) > 0){
	foreach($his_friends as $friend){
    echo "<div class = \"foto_users\">
				       <div class = \"oval\" style =\"background-image: url(images/";
					   if(file_exists("images/$friend[friend_id].jpg")){
		 echo $friend['friend_id'];} else {echo 'standard';}
		 echo ".jpg)\"></div>
					   <div class = \"right\">
					    <p><a href =\"different_page.php?&id=$friend[friend_id]\" target=\"_blank\">$friend[friend]</a></p>
					    <p>{$category[$friend['status']]}</p>
						<p><a href=\"?&out_friend=$friend[friend_id]\">Удалить из друзей</a></p>
					   </div>	
					  </div>";
			}
			}
		echo "</div>";			
?>
          </div>
		  <div class = "subscriptions">
<?php
	if(!empty($_GET['more_his_subscribers'])){
	echo '<p><a href ="?&less_his_subscribers=1">Скрыть моих подписчиков</a></p>';}
	if(!empty($_GET['less_his_subscribers'])){
	echo '<p><a href ="?&more_his_subscribers=1">Показать моих подписчиков</a></p>';}	
	if(empty($_GET['less_his_subscribers']) && empty($_GET['more_his_subscribers'])){	  
	echo '<p><a href ="?&more_his_subscribers= 1">Показать моих подписчиков</a></p>';}
	
    include('baza.php');
	
	$query = "SELECT subscriber, society_users.login AS subscribers, society_users.status AS status FROM society_friends 
    LEFT JOIN society_users ON society_friends.subscriber = society_users.id
    WHERE user_id = '$my_foto' AND subscriber > 0
    ";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($his_subscribers = []; $row = mysqli_fetch_assoc($result); $his_subscribers[] = $row);
	//var_dump($his_subscribers);	
	
		echo "<div class =\"";
		
			if(!empty($_GET['more_his_subscribers'])){echo 'show';} else {echo 'hidden';}

		echo "\">";

    if(count($his_subscribers) == 0){echo 'У Вас нет подписчиков';}
	
	if(count($his_subscribers) > 0){
	foreach($his_subscribers as $subscriber){
    echo "<div class = \"foto_users\">
				       <div class = \"oval\" style =\"background-image: url(images/";
					   if(file_exists("images/$subscriber[subscriber].jpg")){
		 echo $subscriber['subscriber'];} else {echo 'standard';}
		 echo ".jpg)\"></div>
					   <div class = \"right\">
					    <p><a href =\"different_page.php?&id=$subscriber[subscriber]\" target=\"_blank\">$subscriber[subscribers]</a></p>
					    <p>{$category[$subscriber['status']]}</p>
						<p><a href=\"?&out_subscriber=$subscriber[subscriber]\">Удалить из подписчиков</a></p>
					   </div>	
					  </div>";
			}
	}
		echo "</div>";		
?>		  
		  </div>
		</div>
	   </main>	
	   <main class = "primary_three">
		<div class = "info">
		 <a class ="custom" href ="edit_custom.php">Настройки</a>
		 <p><?php echo $_SESSION['login']; ?></p>
		 <p class = "language">
		 Язык: 
	<?php
		include('baza.php');
		
		$id = $_SESSION['user_id'];
		
		$query = "SELECT main_quote, society_language.language AS my_language, society_political.politics, selfview, society_worldview.view, society_main_thing.things, society_main_in_people.quality, society_smoking.smok, society_alcohol.alco, inspire, quote FROM `society_info` 
		LEFT JOIN society_language ON society_info.language = society_language.id 
		LEFT JOIN society_political ON society_info.political = society_political.id 
		LEFT JOIN society_worldview ON society_info.worldview = society_worldview.id 
		LEFT JOIN society_main_thing ON society_info.main_thing = society_main_thing.id 
		LEFT JOIN society_main_in_people ON society_info.main_in_people = society_main_in_people.id 
		LEFT JOIN society_smoking ON society_info.smoking = society_smoking.id 
		LEFT JOIN society_alcohol ON society_info.alcohol = society_alcohol.id 
		WHERE society_info.user_id = $id";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		$total = mysqli_fetch_assoc($result);
		echo $total['my_language'];
		echo "<p>{$total['main_quote']}</p>";
		//var_dump($total);
	?>	
		</p>
	<?php 
		if(!empty($_GET['more'])){
		echo '<p><a href ="?&less=1">Скрыть</a></p>';}
		if(!empty($_GET['less'])){
		echo '<p><a href ="?&more=1">Показать подробную информацию</a></p>';}
		if(empty($_GET['less']) && empty($_GET['more'])){	  
		echo '<p><a href ="?&more= 1">Показать подробную информацию</a></p>';}	
	?>
		 <div class = "<? if(!empty($_GET['more'])){echo 'show';} else {echo 'hidden';} ?>">
		  <p class = "custom"><a href ="edit_info.php">Редактировать личную информацию</a></p>
	<?php 
		if((trim($total['politics'])) !== 'Не выбрано'){echo "<p>Политические предпочтения: $total[politics]</p>";}
		if((strlen(trim($total['selfview']))) > 0){echo "<p>Мировоззрение: $total[selfview]</p>";} elseif((trim($total['view'])) != 'Не выбрано' && (strlen(trim($total['selfview']))) < 1) {echo "<p>Мировоззрение: $total[view]</p>";}
		if((trim($total['things'])) != 'Не указано'){echo "<p>Главное в жизни: $total[things]</p>";}
		if((trim($total['quality'])) != 'Не указано'){echo "<p>Главное в людях: $total[quality]</p>";}
		if((trim($total['smok'])) != 'Не указано'){echo "<p>Отношение к курению: $total[smok]</p>";}
		if((trim($total['alco'])) != 'Не указано'){echo "<p>Отношение к алкоголю: $total[alco]</p>";}
		if(strlen(trim($total['inspire'])) > 0){echo "<p>Вдохновляет: $total[inspire]</p>";}
		if(strlen(trim($total['quote'])) > 0){echo "<p>Любимые цитаты: $total[quote]</p>";}
		if(trim($total['politics']) == 'Не выбрано' && trim($total['view']) == 'Не выбрано' && strlen(trim($total['selfview'])) < 1 && trim($total['things'])== 'Не указано' && trim($total['quality']) == 'Не указано' && trim($total['smok']) == 'Не указано' && trim($total['alco']) == 'Не указано' && strlen(trim($total['inspire'])) < 1 && strlen(trim($total['quote'])) < 1){echo '<p>Отсутствует доступная информация</p>';}
	?>
		 </div>
		 <p class = "count_friends"></p>
		 <p class = "count_foto"></p>
		</div> 
		<div class = "my_foto">
		my_foto
		</div>
	<?php 

		if(!empty($_GET['no_what'])){
		echo '<p class ="what_new"><a href ="?&what=1">Что у Вас нового?</a></p>';}
		if(empty($_GET['no_what']) && empty($_GET['what'])){	  
		echo '<p class ="what_new"><a href ="?&what= 1">Что у Вас нового?</a></p>';}	
	?>		
		<div class = "news <? if(!empty($_GET['what'])){echo 'show';} else {echo 'hidden';} ?>">
	<?php 
		if(!empty($_GET['what'])){
		echo '<p><a href ="?&no_what=1"><img class = "close" src = "images/krest.png" alt = "close"></a></p>';}
?>		
         <form action="" method="POST">
		  <br>
		  <p><input class = "news_head" type = "text" name = "head" placeholder = "Тема сообщения"></p>
		  <br>
		  <p><textarea class = "news_text" name = "text" placeholder = "Ваше сообщение"></textarea></p>
		  <br>
		  <p><input name="public" type="submit" value="Опубликовать"></p>
		  <br>
		 </form> 
<?php
    if(!empty($_POST['public'])){
    $news_head = $_POST['head'];
	$news_text = $_POST['text'];
	$date = date('Y-m-d H:i:s', time());
	
    include('baza.php');
	
	$query = "INSERT INTO society_bands SET head='$news_head', text='$news_text', date='$date', user_id='$id', author='$id'";
    $result = mysqli_query($link, $query) or die(mysqli_error($link)); 
	}	
?>			 
		</div>
	<div class = "band">
<?php
    if(!empty($_POST['answer_band'])){
	
	$answer_text = $_POST['answer'];
	$bband = $_POST['bband'];
	$answer_date = date('Y-m-d H:i:s', time());
	
    include('baza.php');

    $query = "INSERT INTO society_comments SET band_id = '$bband', text ='$answer_text', date='$answer_date', user_id='$id'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	}
	
    include('baza.php');
	
	$query = "SELECT society_bands.id, head, text, date, society_users.login AS author FROM society_bands  
    LEFT JOIN society_users ON society_bands.author = society_users.id
    WHERE user_id = '$id'
    ORDER BY date DESC";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($bands = []; $rows = mysqli_fetch_assoc($result); $bands[] = $rows);
	//var_dump($bands);
	
	foreach($bands as $band){
		
	$band_id = $band['id'];
	
	include('baza.php');	
	
    $query = "SELECT COUNT(*) AS count FROM society_comments WHERE society_comments.band_id = '$band_id'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$count_comments = mysqli_fetch_assoc($result)['count'];
	//var_dump($count_comments);
	
		echo "<div class = \"ash\">
		       <p>$band[date]</p>
		       <h4>$band[head]</h4>
			   <p>Пользователь: $band[author]</p>
			   <p>$band[text]</p>
			   <p class = \"counter\"><a href = \"?&more_comments=$band[id]\"><img class = \"comments\" src = \"images/comments.png\" alt = \"comments\"></a></p>
			   <p class = \"counter\">"; if($count_comments > 0){echo "$count_comments";} 
			   echo "</p>
			    <div class = "; if(!empty($_GET['more_comments'])){echo 'show';} else {echo 'hidden';}
   
			   echo "\"</div>";
			   
               if(!empty($_GET['more_comments']) && $_GET['more_comments'] == $band['id']){
			   
			   include('baza.php');
			   
			   $query = "SELECT society_comments.text, society_users.login, society_comments.date FROM society_comments 
               LEFT JOIN society_users ON society_comments.user_id = society_users.id
               WHERE society_comments.band_id = '$band_id'
               GROUP BY society_comments.date ASC";
			   $result = mysqli_query($link, $query) or die(mysqli_error($link));
			   for($commentss = []; $rows = mysqli_fetch_assoc($result); $commentss[] = $rows);
			   //var_dump($commentss);
			   
			   if(!empty($commentss)){
				   foreach($commentss as $comment){
					   echo "
					         <div>
					         <p>Дата: $comment[date]</p>
					         <p>Пользователь: $comment[login]</p>
							 <p>Сообщение: $comment[text]</p>
							 </div>";
				   }   echo "<br>
							 <form action=\"\" method=\"POST\">
							  <p><textarea class = \"news_text\" name = \"answer\" placeholder = \"Написать комментарий\"></textarea></p>
							  <br>
							  <p><input name=\"bband\" type=\"hidden\" value=\"$band_id\"></p>
							  <p><input name=\"answer_band\" type=\"submit\" value=\"Ответить\"></p>
							  <br>
							  </form>";
			   } else {echo "Пока данную запись никто не комментировал. Будьте первым!
			                 <br><br>
							 <form action=\"\" method=\"POST\">
							  <p><textarea class = \"news_text\" name = \"answer\" placeholder = \"Ваше сообщение\"></textarea></p>
							  <br>
							  <p><input name=\"bband\" type=\"hidden\" value=\"$band_id\"></p>
							  <p><input name=\"answer_band\" type=\"submit\" value=\"Ответить\"></p>
							  <br>
							  </form>";}
               echo "
			   <p><a href =\"?&less_comments=$band[id]\">Скрыть комментарии</a></p>";}echo "					
			   </div>";
	}	
?>
		</div>	
	   </main>
	   <footer>
		<p><img src = "images/society.png" alt = "society"></p>
		<p>Copyright © 2001 - 2021  Society.yes</p>
	   </footer>
   </div>	   
  </div>
<?php
  }
?>
 </body>   
</html>   