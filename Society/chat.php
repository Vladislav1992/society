<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Социальная сеть </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=205" type="text/css"> 
 </head>
 <body>
<?php
  if(empty($_SESSION['auth'])){
	  echo '<div class = "alert">
	        <h4>Чтобы просматривать эту страницу, нужно зайти на сайт.</h4>
			<br>
            <p><a href = "admin/register.php">Регистрация</a></p>
			<br>
            <p><a href = "admin/login.php">Авторизация</a></p>
            </div>';} else {
?>
  <div class = "wrapper">
   <div class = "wrapper_two">   
	   <main class = "primary_one">
		<div class = "sidebar">
		 <p><a href = "/">Моя страница</a></p>
		 <p><a href = "admin/logout.php">Выйти</a></p>
<?php
            $my_foto = $_SESSION['user_id'];
					
			//соглашение с уведомлением о том что меня добавили в друзья
			if(!empty($_GET['ok']) && $_GET['ok'] == 2){
			$yes_friend = $_GET['yes_friend'];
			
			include('baza.php');
			
			$query = "UPDATE society_friends SET request_answer = 0 
			WHERE user_id = '$my_foto' AND friend_id = '$yes_friend' AND request_answer = 1";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//отписка
			if(!empty($_GET['unsubscribe']) && $_GET['unsubscribe'] == 1){
			$unsubscribe_no_friend = $_GET['unsubscribe_no_friend'];
			
			include('baza.php');
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$my_foto' AND request_id = '$unsubscribe_no_friend' AND request_answer = 1";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$unsubscribe_no_friend' AND subscriber = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));			
			}
			
			//соглашение с уведомлением о том что меня оставили в подписчиках
			if(!empty($_GET['ok']) && $_GET['ok'] == 1){
			$no_friend = $_GET['no_friend'];
			
			include('baza.php');
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$my_foto' AND request_id = '$no_friend' AND request_answer = 1";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//отмена заявки в друзья
			if(!empty($_GET['not']) && $_GET['not'] == 1){
			$del_friend = $_GET['del_friend'];
			
			include('baza.php');
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$del_friend' AND subscriber = '$my_foto' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			
			$query = "DELETE FROM society_friends 
			WHERE user_id = '$my_foto' AND request_id = '$del_friend'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//оставить в подписчиках
			if(!empty($_GET['not']) && $_GET['not'] == 2){
			$new_subscriber = $_GET['new_subscriber'];
			
			include('baza.php');
			
			$query = "UPDATE society_friends SET request_id=0 
			WHERE user_id = '$my_foto' AND subscriber = '$new_subscriber' AND request_id = '$new_subscriber'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));

			$query = "UPDATE society_friends SET request_answer=1  
			WHERE user_id = '$new_subscriber' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
			//добавить в друзья
			if(!empty($_GET['yes']) && $_GET['yes'] == 1){
			$new_friend = $_GET['new_friend'];
			
			include('baza.php');
			
			$query = "UPDATE society_friends SET friend_id = '$new_friend', subscriber=0, request_id=0 
			WHERE user_id = '$my_foto' AND subscriber = '$new_friend' AND request_id = '$new_friend'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			
			$query = "UPDATE society_friends SET friend_id='$my_foto', request_id=0, request_answer = 1 
			WHERE user_id = '$new_friend' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			}
			
		if(!empty($_GET['more_news'])){
		echo '<p><a href ="?&less_news=1">Скрыть новости и уведомления></a></p>';}
		if(!empty($_GET['less_news'])){
		echo '<p><a href ="?&more_news=1">Показать новости и уведомления</a></p>';}	
		if(empty($_GET['less_news']) && empty($_GET['more_news'])){	  
		echo '<p><a href ="?&more_news= 1">Показать новости и уведомления</a></p>';}	
		
        include('baza.php');
		
		$query = "SELECT * FROM society_friends WHERE user_id='$my_foto' AND request_id > 0 OR subscriber='$my_foto' AND request_id = '$my_foto' OR user_id='$my_foto' AND request_answer = 1";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
	    for($news = []; $row = mysqli_fetch_assoc($result); $news[] = $row);
		//var_dump($news);

		echo "<div class =\"";
		
			if(!empty($_GET['more_news'])){echo 'show';} else {echo 'hidden';}

		echo "\">";
		
		if(count($news) == 0){
			echo 'Новостей и новых уведомлений нет';
		}
		
		foreach($news as $breeze){
			//меня оставили в подписчиках
			if($breeze['user_id'] == $my_foto && $breeze['request_id'] != $my_foto && $breeze['request_answer'] == 1){
			$other = $breeze['request_id'];
			
			include('baza.php');
			
		    $query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.request_id = society_users.id
			WHERE user_id = '$my_foto' AND request_id = '$other' AND request_answer = 1";
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];	
			
			if(strlen($he) > 0){
			echo "<div> Пользователь <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a> решил оставить Вас в подписчиках
			<p><a href = \"?&ok=1&no_friend=$other\">Ok</a></p>
			<p><a href = \"?&unsubscribe=1&unsubscribe_no_friend=$other\">Отписаться от $he</a></p>
			</div>";
			}			
			}
			
			//меня добавили в друзья
			if($breeze['user_id'] == $my_foto && $breeze['friend_id'] != $my_foto && $breeze['request_answer'] == 1){
			$other = $breeze['friend_id'];
			
			include('baza.php');
			
		    $query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.friend_id = society_users.id
			WHERE user_id = '$my_foto' AND friend_id = '$other' AND request_answer = 1";
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];	
			
			if(strlen($he) > 0){
			echo "<div> Пользователь <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a> добавил Вас в друзья
			<p><a href = \"?&ok=2&yes_friend=$other\">Ok</a></p>
			</div>";	
			}			
			}			
			
			//я подал заявку в друзья
			if($breeze['user_id'] != $my_foto && $breeze['request_id'] == $my_foto && $breeze['request_id'] == $breeze['subscriber']){
			$other = $breeze['user_id'];
			
			include('baza.php');
				
			$query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.user_id = society_users.id
			WHERE user_id = '$other' AND request_id = '$my_foto'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];
			
			if(strlen($he) > 0){
			echo "<div> Вы подали заявку в друзья пользователю <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a>
			<p><a href = \"?&not=1&del_friend=$other\">Отменить заявку в друзья</a></p></div>";
			}
			}
			
			//мне подали заявку в друзья
			if($breeze['user_id'] = $my_foto && $breeze['request_id'] != $my_foto && $breeze['request_id'] == $breeze['subscriber']){	
            $other = $breeze['request_id'];		

            include('baza.php');			
			
			$query = "SELECT * FROM society_friends 
			LEFT JOIN society_users ON society_friends.request_id = society_users.id
			WHERE user_id = '$my_foto' AND request_id = '$other'";	
			$result = mysqli_query($link, $query) or die(mysqli_error($link));
			$he = mysqli_fetch_assoc($result)['login'];
			//var_dump($he);
			
			if(strlen($he) > 0){
			echo "<div> Вам подал заявку в друзья пользователь <a href = \"different_page.php?&id=$other\" target=\"_blank\">$he</a>
			<p><a href = \"?&not=2&new_subscriber=$other\">Оставить в подписчиках</a></p>
			<p><a href = \"?&yes=1&new_friend=$other\">Добавить в друзья</a></p></div>";
			}
			}			
		}
		
		echo "</div>";	
		
		$category = [1 => 'юзер', 2 => 'модератор', 3 => 'администратор'];
?>
		 <p><a href = "chat_index.php">Мессенджер</a></p>
		 <p><a href = "#">Друзья</a></p>
		 <p><a href = "#">Сообщества</a></p>
		 <p><a href = "#">Фотографии</a></p>
		 <p><a href = "#">Поиск</a></p>
		 <br>
		 <form action="" method="POST">
		  <p><input name="search_friends" type="text" placeholder="Найти человека"></p>
		  <br>
		  <p><input name="search" type="submit" value="Найти"></p>
		  <br>
		 </form>
<?php
    include('baza.php');
	
    if(!empty($_POST['search_friends'])){
		$name = $_POST['search_friends'];
		
		$query = "SELECT id, login, status FROM society_users WHERE login='$name'";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		for($friends = []; $row = mysqli_fetch_assoc($result); $friends[] = $row);
		
		include('search_friends_chat.php');
		} //else{	
	
    if(isset($_POST['search']) && empty($_POST['search_friends'])){	
		$query = "SELECT id, login, status FROM society_users";
		$result = mysqli_query($link, $query) or die(mysqli_error($link));
		for($friends = []; $row = mysqli_fetch_assoc($result); $friends[] = $row);
		
		include('search_friends_chat.php');
		}
        //var_dump($friends);	
?>
		</div> 
	   </main>	
	   <main class = "primary_two">
		<div class = "primary_foto">
		 <div class = "in_foto"	style ="background-image: url(images/<? 
		 if(file_exists("images/$my_foto.jpg")){
		 echo $my_foto;} else {echo 'standard';}?>.jpg)">
		 </div>
		</div>		
		<div class = "friends">
		  <div class = "his_friends">
<?php
	if(!empty($_GET['more_his_friends'])){
	echo '<p><a href ="?&less_his_friends=1">Скрыть моих друзей</a></p>';}
	if(!empty($_GET['less_his_friends'])){
	echo '<p><a href ="?&more_his_friends=1">Показать моих друзей</a></p>';}	
	if(empty($_GET['less_his_friends']) && empty($_GET['more_his_friends'])){	  
	echo '<p><a href ="?&more_his_friends= 1">Показать моих друзей</a></p>';}
	
    include('baza.php');
	
	$query = "SELECT friend_id, society_users.login AS friend, society_users.status AS status FROM society_friends 
    LEFT JOIN society_users ON society_friends.friend_id = society_users.id
    WHERE user_id = '$my_foto' AND friend_id > 0
    ";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($his_friends = []; $row = mysqli_fetch_assoc($result); $his_friends[] = $row);
	//var_dump($his_friends);
	
		echo "<div class =\"";
		
			if(!empty($_GET['more_his_friends'])){echo 'show';} else {echo 'hidden';}

		echo "\">";
		
	if(count($his_friends) == 0){echo 'У Вас нет друзей';}
	
	if(count($his_friends) > 0){
	foreach($his_friends as $friend){
    echo "<div class = \"foto_users\">
				       <div class = \"oval\" style =\"background-image: url(images/";
					   if(file_exists("images/$friend[friend_id].jpg")){
		 echo $friend['friend_id'];} else {echo 'standard';}
		 echo ".jpg)\"></div>
					   <div class = \"right\">
					    <p><a href =\"different_page.php?&id=$friend[friend_id]\" target=\"_blank\">$friend[friend]</a></p>
					    <p>{$category[$friend['status']]}</p>
						<p><a href= \"?&write=$friend[friend_id]\">Написать</a></p>
					   </div>	
					  </div>";
			}
			}
		echo "</div>";			
?>
          </div>
		  <div class = "subscriptions">
<?php
	if(!empty($_GET['more_his_subscribers'])){
	echo '<p><a href ="?&less_his_subscribers=1">Скрыть моих подписчиков</a></p>';}
	if(!empty($_GET['less_his_subscribers'])){
	echo '<p><a href ="?&more_his_subscribers=1">Показать моих подписчиков</a></p>';}	
	if(empty($_GET['less_his_subscribers']) && empty($_GET['more_his_subscribers'])){	  
	echo '<p><a href ="?&more_his_subscribers= 1">Показать моих подписчиков</a></p>';}
	
    include('baza.php');
	
	$query = "SELECT subscriber, society_users.login AS subscribers, society_users.status AS status FROM society_friends 
    LEFT JOIN society_users ON society_friends.subscriber = society_users.id
    WHERE user_id = '$my_foto' AND subscriber > 0
    ";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($his_subscribers = []; $row = mysqli_fetch_assoc($result); $his_subscribers[] = $row);
	//var_dump($his_subscribers);	
	
		echo "<div class =\"";
		
			if(!empty($_GET['more_his_subscribers'])){echo 'show';} else {echo 'hidden';}

		echo "\">";

    if(count($his_subscribers) == 0){echo 'У Вас нет подписчиков';}
	
	if(count($his_subscribers) > 0){
	foreach($his_subscribers as $subscriber){
    echo "<div class = \"foto_users\">
				       <div class = \"oval\" style =\"background-image: url(images/";
					   if(file_exists("images/$subscriber[subscriber].jpg")){
		 echo $subscriber['subscriber'];} else {echo 'standard';}
		 echo ".jpg)\"></div>
					   <div class = \"right\">
					    <p><a href =\"different_page.php?&id=$subscriber[subscriber]\" target=\"_blank\">$subscriber[subscribers]</a></p>
					    <p>{$category[$subscriber['status']]}</p>
					   </div>	
					  </div>";
			}
	}
		echo "</div>";		
?>		  
		  </div>
		</div>
	   </main>	
	   <main class = "primary_three">	
<?php
    if(!empty($_GET['write'])){
	$companion = $_GET['write'];
		
	include('baza.php');
	
    $query = "SELECT * FROM society_chats WHERE user_id='$my_foto' AND companion_id='$companion'";	
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$res = mysqli_fetch_assoc($result);
    //var_dump($res);
	
	$query = "SELECT * FROM society_chats WHERE user_id='$companion' AND companion_id='$my_foto'";	
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$ser = mysqli_fetch_assoc($result);
    //var_dump($res);
	
	if(empty($res)){
	
	include('baza.php');
	
	$query = "INSERT INTO society_chats SET user_id='$my_foto',  companion_id='$companion'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	} 
	
	if(empty($ser)){
	
	include('baza.php');
	
	$query = "INSERT INTO society_chats SET user_id='$companion',  companion_id='$my_foto'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	} 	
	} 
	
	if(!empty($_GET['del_chat'])){
	$del = $_GET['del_chat'];
		
	include('baza.php');
	
	$query = "DELETE FROM society_chats WHERE user_id='$my_foto' AND companion_id='$del'";	
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	}
	
	if(isset($_GET['url'])){
    $url_dialog = $_GET['url'];
    $_SESSION['url'] = $_GET['url'];
    } elseif(!empty($_SESSION['url'])){
    $url_dialog = $_SESSION['url'];
    }
	//var_dump($_SESSION['url']);
	
	if(!empty($_GET['url'])){
	include('in_chat.php');
	}
	
	if(empty($_GET['url'])){
		
	include('baza.php');
	
    $query = "SELECT * FROM society_chats WHERE user_id='$my_foto'";	
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for($res = []; $row = mysqli_fetch_assoc($result); $res[] = $row);
	//var_dump($res);
	
	foreach($res as $elem){
	$login = $elem['companion_id'];
		
	include('baza.php');
	
	$query = "SELECT login, id FROM society_users WHERE id='$login'";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$name = mysqli_fetch_assoc($result);	
	
	echo "<div class=\"dialog\">
	       <div class = \"foto_dialog\"	style =\"background-image: url(images/"; 
		 if(file_exists("images/$name[id].jpg")){
		 echo $name['id'];} else {echo 'standard';}
		 echo ".jpg)\"></div>";
	
    include('baza.php');

    $query = "SELECT text, user_id, date, society_users.login AS login FROM society_chat
    LEFT JOIN society_users ON society_chat.user_id = society_users.id
	WHERE user_id='$my_foto' AND companion_id='$login' OR user_id='$login' AND companion_id='$my_foto'
    ORDER BY date DESC LIMIT 1";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	$text = mysqli_fetch_assoc($result);
	//var_dump($text);
    
	if(empty($text)){
	echo "<p><a class = \"left_dialog\" href =\"?&url=$name[id]\">Начать чат</a></p>";	
	} else {
	echo "<div class = \"right_dialog\">$text[date]</div>
          <p><a class = \"left_dialog\" href =\"?&url=$name[id]\">$text[text]</a></p>";
	}
		  
	 echo "<p>$name[login]</p>
		   <p><a href=\"?&del_chat=$name[id]\">Удалить чат</a></p>
		  </div>";	
	}
	}
?>	   
	   </main> 
	   <footer>
		<p><img src = "images/society.png" alt = "society"></p>
		<p>Copyright © 2001 - 2021  Society.yes</p>
	   </footer>
   </div>	   
  </div>
<?php
  }
?>
 </body>   
</html>   