<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Редатирование личной информации </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=196" type="text/css"> 
 </head>
 <body>
  <div class = "wrapper_info">
<?php	
   include('baza.php');
   
   if(!empty($_SESSION['auth'])){
   
    $id = $_SESSION['user_id'];
	
  	$query = "SELECT main_quote, society_language.language AS my_language, society_political.politics, selfview, society_worldview.view, society_main_thing.things, society_main_in_people.quality, society_smoking.smok, society_alcohol.alco, inspire, quote FROM `society_info` 
    LEFT JOIN society_language ON society_info.language = society_language.id 
    LEFT JOIN society_political ON society_info.political = society_political.id 
    LEFT JOIN society_worldview ON society_info.worldview = society_worldview.id 
    LEFT JOIN society_main_thing ON society_info.main_thing = society_main_thing.id 
    LEFT JOIN society_main_in_people ON society_info.main_in_people = society_main_in_people.id 
    LEFT JOIN society_smoking ON society_info.smoking = society_smoking.id 
    LEFT JOIN society_alcohol ON society_info.alcohol = society_alcohol.id 
    WHERE society_info.user_id = $id";
   $result = mysqli_query($link, $query) or die(mysqli_error($link));
   $page = mysqli_fetch_assoc($result);
   
   if(isset($_POST['main_quote']) && isset($_POST['language']) && isset($_POST['political']) && isset($_POST['selfview']) && isset($_POST['worldview']) && isset($_POST['main_thing']) && isset($_POST['main_in_people']) && isset($_POST['smoking']) && isset($_POST['alcohol']) && isset($_POST['inspire']) && isset($_POST['quote'])){
   $main_quote = $_POST['main_quote'];
   $language = $_POST['language']; 
   $political = $_POST['political']; 
   $selfview = $_POST['selfview'];
   $worldview = $_POST['worldview'];
   $main_thing = $_POST['main_thing'];
   $main_in_people = $_POST['main_in_people'];   
   $smoking = $_POST['smoking']; 
   $alcohol = $_POST['alcohol']; 
   $inspire = $_POST['inspire']; 
   $quote = $_POST['quote']; 
   } else {
   $main_quote = $page['main_quote'];
   $language = $page['my_language']; 
   $political = $page['politics']; 
   $selfview = $page['selfview'];
   $worldview = $page['view'];
   $main_thing = $page['things'];
   $main_in_people = $page['quality'];
   $smoking = $page['smok'];
   $alcohol = $page['alco'];
   $inspire = $page['inspire'];
   $quote = $page['quote'];
   }

   $languages = [0 => 'English', 1 => 'Russian'];
   
   $politics = [0 => 'Не выбрано', 1 => 'Индифферентные', 2 => 'Коммунистические', 3 => 'Социалистические', 4 => 'Умеренные', 5 => 'Либеральные', 6 => 'Консервативные', 7 => 'Монархические', 8 => 'Ультраконсервативные', 9 => 'Либертарианские'];
   
   $worldviews = [0 => 'Не выбрано', 1 => 'Иудаизм', 2 => 'Православие', 3 => 'Католицизм', 4 => 'Протестантизм', 5 => 'Ислам', 6 => 'Буддизм', 7 => 'Конфуцианство', 8 => 'Светский гуманизм', 9 => 'Пастафарианство'];
   
   $things = [0 => 'Не указано', 1 => 'Семья и дети', 2 => 'Карьера и деньги', 3 => 'Развлечения и отдых', 4 => 'Наука и исследования', 5 => 'Совершенствование мира', 6 => 'Саморазвитие', 7 => 'Красота и искусство', 8 => 'Слава и влияние'];
   
   $quality = [0 => 'Не указано', 1 => 'Ум и креативность', 2 => 'Доброта и честность', 3 => 'Красота и здоровье', 4 => 'Власть и богатство', 5 => 'Смелость и упорство', 6 => 'Юмор и жизнелюбие'];
   
   $smok_and_alco = [0 => 'Не указано', 1 => 'Резко негативное', 2 => 'Негативное', 3 => 'Компромиссное', 4 => 'Нейтральное', 5 => 'Положительное'];
   
   function func($arr, $str)
   {
	   $i = 0;
	   
	   for($i = 0; $i < count($arr); $i++){
		   if($arr[$i] == $str){
		   echo "<option selected value=$i>$arr[$i]</option>";} else {
			   echo "<option value=$i>$arr[$i]</option>";}
	   }
   }
	   
   echo '	
   <form method="POST">
   Заглавная цитата:<br>
    <textarea class="edit" name="main_quote" placeholder="заглавная цитата">'.$main_quote.'</textarea><br><br>
	Язык:<br>
    <select name="language">';
	func($languages, $language);
	echo '</select><br><br>
	Политические предпочтения:<br>
    <select name="political">';
	func($politics, $political);
	echo '</select><br><br>
	Мировоззрение(напишите свой выбор или выберите из доступных вариантов):<br>
    <textarea class="edit" name="selfview" placeholder="личное видение мира">'.$selfview.'</textarea><br><br>
	Варианты:<br>
	<select name="worldview">'; 
	func($worldviews, $worldview);
	echo '</select><br><br>
	Главное в жизни:<br>
    <select name="main_thing">';
	func($things, $main_thing);
	echo '</select><br><br>
	Главное в людях:<br>
    <select name="main_in_people">';
	func($quality, $main_in_people);
	echo '</select><br><br>
	Отношение к курению:<br>
    <select name="smoking">';
	func($smok_and_alco, $smoking);
	echo '</select><br><br>	
	Отношение к алкоголю:<br>
	<select name="alcohol">';
	func($smok_and_alco, $alcohol);
	echo '</select><br><br>	
	Вдохновляет:<br>
	<textarea class="edit" name="inspire" placeholder="вдохновляет">'.$inspire.'</textarea><br><br>	
	Любимые цитаты:<br>
	<textarea class="edit" name="quote" placeholder="любимые цитаты">'.$quote.'</textarea><br><br>	
    <input name="edit" type="submit" value="Сохранить">
	<br><br>
   </form>';
   echo '<p><a href ="/">Вернуться на главную страницу</a></p>
   <br>
   ';

   if(!empty($_POST['edit'])){
   if(isset($_POST['main_quote']) && isset($_POST['language']) && isset($_POST['political']) && isset($_POST['selfview']) && isset($_POST['worldview']) && isset($_POST['main_thing']) && isset($_POST['main_in_people']) && isset($_POST['smoking']) && isset($_POST['alcohol']) && isset($_POST['inspire']) && isset($_POST['quote'])){
	   
   $main_quote = $_POST['main_quote'];
   $language = $_POST['language']; 
   $political = $_POST['political']; 
   $selfview = $_POST['selfview'];
   $worldview = $_POST['worldview'];
   $main_thing = $_POST['main_thing'];
   $main_in_people = $_POST['main_in_people'];   
   $smoking = $_POST['smoking']; 
   $alcohol = $_POST['alcohol']; 
   $inspire = $_POST['inspire']; 
   $quote = $_POST['quote'];
   
   $query = "UPDATE society_info SET main_quote='$main_quote', language='$language', political='$political', selfview='$selfview', worldview='$worldview', main_thing='$main_thing', main_in_people='$main_in_people', smoking='$smoking', alcohol='$alcohol', inspire='$inspire', quote='$quote' WHERE user_id='$id'";
   mysqli_query($link, $query) or die(mysqli_error($link)); 

   }
   }
   }
?>
</div>